package com.springio.com.SpringApp;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GSFetcherService {
	
	@Autowired
	private PlantGenomeSequenceRepo repo_instance ;

	PlantGenomeRecord p1 = new PlantGenomeRecord("Arabidopsis thaliana", "thale cress" , null , "The Arabidopsis Information Resource (TAIR)" , "PRJNA10719");
	PlantGenomeRecord p2 = new PlantGenomeRecord("Arabidopsis thaliana", "thale cress" , null , "The Arabidopsis Information Resource (TAIR)" , "PRJNA101719");
	PlantGenomeRecord p3 = new PlantGenomeRecord("Arabidopsis thaliana", "thale cress" , null , "The Arabidopsis Information Resource (TAIR)" , "PRJNA107319");

	List<PlantGenomeRecord> totalList = new ArrayList<PlantGenomeRecord>();
	List<PlantGenomeRecord> resultlist;
	Iterable<PlantGenomeRecord> t;

	private List<PlantGenomeRecord> prepareresult(Iterable<PlantGenomeRecord> t)
	{
		resultlist = new ArrayList<PlantGenomeRecord>();
		for (PlantGenomeRecord plantGenomeRecord : t) 
		 {
			resultlist.add(plantGenomeRecord);
		 } 
		
		 return resultlist;
	}
	
	// Constrrcuter ,which also populates our demo database
	public GSFetcherService() 
	{
		super();
		totalList.add(p1);
		totalList.add(p2);
		totalList.add(p3);
	}
	
	
	
	public List<PlantGenomeRecord> getAllReleasedateAsc()
	{
		resultlist = new ArrayList<>();
	    return(repo_instance.findAllByOrderByReleasedateDesc());

	}

	// Method to get all values 
	public List<PlantGenomeRecord> getAll()
	{
		 return prepareresult(repo_instance.findAll());
	}

	// Method to get results by projectid
	public List<PlantGenomeRecord> getByProjectId(String projectId) 
	{
		// TODO Auto-generated method stub
		return prepareresult(repo_instance.findByProjectId(projectId));
	}
	
	public List<PlantGenomeRecord> getByOrganismName(String organismName)
	{
		return prepareresult(repo_instance.findByOrganismName(organismName));
	}
	
	public List<PlantGenomeRecord> getByCommonName(String CommonName)
	{
		return prepareresult(repo_instance.findByCommonName(CommonName));	
	}
}
