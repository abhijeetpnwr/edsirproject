package com.springio.com.SpringApp;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PlantGenomeRecord 
{

	@Id
	private int id;
	
	private String projectId;
	private String organismName,commonName,strain,dataSource,RefSeq_FTP,publication,GenBank_FTP;
	private float size;
	int genes;
	Date releasedate;
	
	public PlantGenomeRecord() {
		super();
	}

	public String getRefSeq_FTP() {
		return RefSeq_FTP;
	}

	public void setRefSeq_FTP(String refSeq_FTP) {
		RefSeq_FTP = refSeq_FTP;
	}

	public String getPublication() {
		return publication;
	}

	public void setPublication(String publication) {
		this.publication = publication;
	}

	public String getGenBank_FTP() {
		return GenBank_FTP;
	}

	public void setGenBank_FTP(String genBank_FTP) {
		GenBank_FTP = genBank_FTP;
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	public int getGenes() {
		return genes;
	}

	public void setGenes(int genes) {
		this.genes = genes;
	}

	public Date getReleasedate() {
		return releasedate;
	}

	public void setReleasedate(Date releasedate) {
		this.releasedate = releasedate;
	}

	public PlantGenomeRecord(String organismName, String commonName, String strain, String dataSource,
			String projectId) {
		super();
		this.organismName = organismName;
		this.commonName = commonName;
		this.strain = strain;
		this.dataSource = dataSource;
		this.projectId = projectId;
	}

	public String getOrganismName() {
		return organismName;
	}

	public void setOrganismName(String organismName) {
		this.organismName = organismName;
	}

	public String getCommonName() {
		return commonName;
	}


	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getStrain() {
		return strain;
	}

	public void setStrain(String strain) {
		this.strain = strain;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	@Override
	public String toString() {
		return "PlantGenomeRecord [projectId=" + projectId + ", organismName=" + organismName + ", commonName="
				+ commonName + ", strain=" + strain + ", dataSource=" + dataSource + ", publication=" + publication
				+ ", size=" + size + ", genes=" + genes + ", releasedate=" + releasedate + "]";
	}
	


}
