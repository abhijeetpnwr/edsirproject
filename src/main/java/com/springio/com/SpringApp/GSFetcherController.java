package com.springio.com.SpringApp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class GSFetcherController {
	
	ModelAndView modelAndView =  null;
	
	@Autowired
	private GSFetcherService f_Instance;
	private List<PlantGenomeRecord> result;
	
	
	private ModelAndView getModelView(Object obj,String viewname,String param,String controller)
	{
		
		modelAndView = new ModelAndView();
		modelAndView.addObject("result", obj);
		modelAndView.addObject("paramName",param);
		modelAndView.addObject("action",controller);
		modelAndView.setViewName(viewname);
		System.out.println(result);
		return modelAndView;
	}
	
	@RequestMapping("/")
	public String getRoot()
	{			
		
		return "index";
	}
	
	@RequestMapping("home")
	public String getHome()
	{			
		
		return "index";
	}
	
	@RequestMapping("/contact")
	public String contactUs()
	{			
		
		return "contact";
	}
	
	
	@RequestMapping("getbyorganismview")
	public ModelAndView getByOrganismNameView()
	{			
		return getModelView(null,"search", "organismname","getByOrganismName");
		
	}
	
	@RequestMapping("getbycommonnameview")
	public ModelAndView getSearchByCommonNameView()
	{			
		return getModelView(null, "search", "commonname","getByCommonName");
	}
	
	@RequestMapping("getbyLabIdView")
	public ModelAndView getbyLabIdView()
	{			
		
		return getModelView(null, "search", "project_id","getByProjectId");
	}
	
	@RequestMapping("getbyProjectIdView")
	public ModelAndView getSearchByProjectname()
	{			
		
		return getModelView(null, "search", "project_id","getByProjectId");
	}
	
	
	//**
	
	
	
	
	
	//**
	
	
	@RequestMapping("getAllBySubmissionDate")
	public  ModelAndView  getAllbyReleaseDate()
	{			
		result = f_Instance.getAllReleasedateAsc(); // It will get all values from Service Method
		return getModelView(result, "showresult", null, null);
	}
	
	

	
	// This method should return the whole table to interface
	@RequestMapping("getGSList")
	public ModelAndView getAll()
	{
		System.out.println("I will return the full list to user");				
		result =  f_Instance.getAll(); // It will get all values from Service Method
		return getModelView(result,"showresult",null,null);
	}
	
	// This method should return results fine by organism name
	@RequestMapping("getByOrganismName")
	public ModelAndView getByorganismName(@RequestParam("organismname") String organismname)
	{
		System.out.println("I reached here . heyy ....");
		result = f_Instance.getByOrganismName(organismname) ; // It will get all values from Service Method	
		return getModelView(result,"showresult",null,null);
	}
	
	@RequestMapping("getByCommonName")
	public ModelAndView getByCommonName(@RequestParam("commonname") String commonname)
	{
		System.out.println("I reached here . heyy ....");
		result = f_Instance.getByCommonName(commonname) ; // It will get all values from Service Method	
		return getModelView(result,"showresult",null,null);
	}
	
	
	// This method should return the whole table to interface
	@RequestMapping("getByProjectId")
	public ModelAndView getByProjectId(@RequestParam("project_id") String project_Id)
	{
		 result = f_Instance.getByProjectId(project_Id) ; // It will get all values from Service Method	
		 return  getModelView(result, "showresult", null, null);
	}
	


}
