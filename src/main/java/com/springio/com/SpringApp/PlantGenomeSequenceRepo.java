package com.springio.com.SpringApp;

import java.util.List;

import org.springframework.data.repository.CrudRepository;



public interface PlantGenomeSequenceRepo extends CrudRepository<PlantGenomeRecord,String>
{
	public List<PlantGenomeRecord> findAllByOrderByReleasedateDesc();
 	
	public List<PlantGenomeRecord>  findByProjectId(String projectId);

	public List<PlantGenomeRecord> findByOrganismName(String organismName);

	public List<PlantGenomeRecord> findByCommonName(String CommonName);
	
}
